from django.db import models

# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name='Категорія')
    parent = models.ForeignKey('self', null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категорія"
        verbose_name_plural = "Категорії"


class Products(models.Model):
    quantity = models.IntegerField(default=0, verbose_name='Кількість')
    category = models.ForeignKey(Category, verbose_name='Категорія')
    price = models.IntegerField(default=0, verbose_name='Ціна')
    title = models.CharField(max_length=200, verbose_name='Назва продукту')
    image = models.ImageField(verbose_name='Зображення')
    description = models.TextField(verbose_name='Опис')

    def __str__(self):
        return '%s %s %s' %(self.title, '->', self.price)

    class Meta:
        verbose_name = "Продукти"
        verbose_name_plural = "Продукт"


class Size(models.Model):
    width = models.IntegerField(default=0, verbose_name='Ширина:')
    height = models.IntegerField(default=0, verbose_name='Довжина:')

    def __str__(self):
        return '%s х %s' %(self.width, self.height)

    class Meta:
        verbose_name = "Розмір"
        verbose_name_plural = "Розміри"


class Sizes(models.Model):
    title = models.CharField(max_length=200, verbose_name='Категорія розміру:')
    sizes = models.ManyToManyField(Size, blank=True, verbose_name='Розміри:')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категорія розміру"
        verbose_name_plural = "Категорія розмірів"


class Color(models.Model):
    title = models.CharField(max_length=200, verbose_name='Колір')

    color = models.CharField(max_length=6, verbose_name='Код кольору')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Колір"
        verbose_name_plural = "Кольори"


class Colors(models.Model):
    title = models.CharField(max_length=200)
    colors = models.ManyToManyField(Color, blank=True, verbose_name='Категорія кольорів')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категорія кольору"
        verbose_name_plural = "Категорії кольорів"